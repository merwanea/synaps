﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Metier
{
    class Mission
    {
        private string nom;
        private string description;
        private int nbHeuresPrevues;
        private Dictionary<DateTime, int> _releveHoraire;
        private Intervenant executant;

        public int NbHeuresPrevues { get => nbHeuresPrevues; set => nbHeuresPrevues = value; }
        public string Description { get => description; set => description = value; }
        public string Nom { get => nom; set => nom = value; }
        public Dictionary<DateTime, int> ReleveHoraire { get => _releveHoraire; set => _releveHoraire = value; }
        internal Intervenant Executant { get => executant; set => executant = value; }

        public void AjouteReleve(DateTime date, int nbHeures)
        {
            this._releveHoraire.Add(date, nbHeures); 
        }

        public int NbHeuresEffectues()
        {
            int nbHeuresEffectuees = 0;
            foreach (KeyValuePair<DateTime, int> kvp in this._releveHoraire)
            {
                int nombreHeuresCourante = kvp.Value;
                nbHeuresEffectuees +=nombreHeuresCourante;
            }
            return nbHeuresEffectuees;
        }
        


    }
}
