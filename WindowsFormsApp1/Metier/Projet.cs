﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Metier
{
    class Projet
    {
        private string nom;
        private DateTime debut;
        private DateTime fin;
        private Decimal prixFactureMO;
        private List<Mission> missions;
        
        public string Nom { get => nom; set => nom = value; }
        public DateTime Debut { get => debut; set => debut = value; }
        public DateTime Fin { get => fin; set => fin = value; }
        public decimal PrixFactureMO { get => prixFactureMO; set => prixFactureMO = value; }
        internal List<Mission> Missions { get => missions; set => missions = value; }

        private decimal CumulCoutMo()
        {
            decimal cumul = 0;
            foreach (Mission m in missions )
            {
                cumul += m.NbHeuresPrevues * m.Executant.TauxHoraire;
            }
            return cumul;
        }

        public decimal MargeBruteCourante()
        {
            decimal marge = 0;
            marge = this.prixFactureMO - this.CumulCoutMo();

            return marge;
        }
    } 
}
