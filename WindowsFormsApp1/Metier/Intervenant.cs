﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Metier
{
    class Intervenant
    {
        private string nom;
        private decimal tauxHoraire;

        public string Nom { get => nom; set => nom = value; }
        public decimal TauxHoraire { get => tauxHoraire; set => tauxHoraire = value; }
    }
}
